﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace MTFS
{
    class MyThreading
    {
        #region Переменные, свойства и конструктор

        private ConcurrentQueue<string> Fifo; // Очередь папок для поиска

        private Search search; // Объявление объекта типа Search

        private string FirstPath = "";// Объявление переменной для пути заданного пользователем

        private string pattern = "";//Переменная для шаблона поиска

        Semaphore semaphore = new Semaphore(1, 1); // Семафор, пока что не используется
        public Search SearchObj => search;

        public ObservableCollection<string> SearchResult { get { return search.Result; } }

        public MyThreading() { Fifo = new ConcurrentQueue<string>(); search = new Search();//Создание обЪекта поиска
        }
            #endregion


            /// <summary>
            /// Публичная функция для создания потоков
            /// </summary>
            /// <param name="n">Количество потоков задаваемое пользователем</param>
            public void CreateThreads(int n, string first_path, string pattern)
        {
            //первичная обработка
            Fifo.Enqueue(first_path);//Закидываем в очередь начальный путь для поиска
            
            if (pattern is null || pattern == "" || pattern == " ") { this.pattern = "*"; }//если паттерн пуст то ставим искать всё
            else { this.pattern = pattern; }


            for (int i = 0; i < n; i++)
                CreateThread();

        }


        /// <summary>
        /// Основная функция, создает поток
        /// </summary>
        private void CreateThread()
        {
            #region Проверяет наличие элементов в очереди
            //Нуждается в доработке на ожидание появления элемента в очереди
            //либо создавать поток не зависимо от наличия элементов в очереди, 
            //который сам будет ждать появления элементов в классе Search
            if (Fifo.TryPeek(out FirstPath)) // Если попытка извлечения была удачной то поток создается
                                             // копируя путь в переменную
            #endregion
            {
                Console.WriteLine($"Dequery result is: {FirstPath}"); //Для отладки
                var a = new Thread(start: () => search.StartSearch(Fifo, this.pattern, Console.Out))
                {
                    IsBackground = true //Делает поток зависимым от основного потока, убрал таким образом метод CollapseAll.
                };//Создает поток, передает
                  //анонимный делегат, в котором функция Начала поиска
                  //с параметами (Очередь, шаблон поиска, вывод консоли(для отладки))
                a.Start();//Запуск потока

            }
            else
            {

                Console.WriteLine("Query is empty");//отладка

            }
        }

    }
}
