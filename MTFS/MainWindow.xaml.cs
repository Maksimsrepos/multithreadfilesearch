﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MTFS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
        MyThreading threading;

        private double refParam = 0;
        private double lala = 0;
        public MainWindow()
        {
            InitializeComponent();

           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            folderBrowser.ShowDialog();
            StartSearchButton.IsEnabled = true;
            ProgressBarN.IsIndeterminate =true;
            Task.Factory.StartNew(() => { lala = Search.GetFolderSize(folder: folderBrowser.SelectedPath, catalogSize: ref refParam); }).ContinueWith(t => { WorkCompleted(); });
           
           
        }
        /// <summary>
        /// Вызывается по завершению расчета байт (Для реализации прогресса поиска)
        /// </summary>
        private void WorkCompleted()
        {
            App.Current.Dispatcher.Invoke(delegate 
                        {
                            ProgressBarN.IsIndeterminate = false;
                            ProgressBarN.Maximum = lala;
                            ProgressBarN.Minimum = 0;
                            Console.WriteLine("Size of root fir: "  + lala);
                            refParam = 0;
                            lala = 0;
                        }                  );
            
           
        }

        private void ButtonStartSearch(object sender, RoutedEventArgs e)
        {
            
            threading = new MyThreading();
            threading.SearchObj.Progress += SearchObj_Progress;
            ProgressBarN.Value = 0;
            threading.CreateThreads(Convert.ToInt32(NumericUD.Value), folderBrowser.SelectedPath, PaternS.Text);
            
            ListViewResult.ItemsSource = threading.SearchResult; // привязка к лист вью элемента ObserbableList

        }

        private void SearchObj_Progress(double progress)
        {
            ProgressBarN.Value += progress;
        }
    }
}
