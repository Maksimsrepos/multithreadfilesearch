﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace MTFS
{
    public delegate void ProgressHandler(double progress);
    public class Search 
    {
        public event ProgressHandler Progress;

        public ObservableCollection<string> Result { get => result; } //(Свойство)Перечесление для сохранения результатов

        private ObservableCollection<string> result;//(Переменная)Перечесление для сохранения результатов

        public Search()
        {
            result = new ObservableCollection<string>();

        }
        /// <summary>
        /// Публичная функция запуска поиска, вызывается из объекта класса Threading,
        /// в методе который создает поток
        /// </summary>
        /// <param name="Fifo">Очередь, содержит папки для поиска</param>
        /// <param name="pattern">Шаблон по которому идет поиск</param>
        /// <param name="cons">Консольный вывод потока, для визуализации отладки потока</param>
        public void StartSearch(IProducerConsumerCollection<string> Fifo, string pattern, TextWriter cons)
        {
            while (Fifo.Count != 0)
            {


                string returnedPath = ""; // Переменная в которую возвращается результат извлечения из очереди 
                Fifo.TryTake(out returnedPath);//Забирает элемент из очереди
                try
                {
                   
                    foreach (var Finded_folder in Directory.EnumerateDirectories(returnedPath, "*", SearchOption.AllDirectories)) // цикл, который выбрасывает найденные папки в очередь,что, в свою очередь, реализует поиск вглубь
                    {
                        //Console.WriteLine(Finded_folder);//Для отладки
                        Fifo.TryAdd(Finded_folder); //Закидываем в очередь найденные папки
                    }

                    foreach (var i in Directory.GetFileSystemEntries(returnedPath, pattern))
                    {
                        FileInfo info;
                        
                            info = new FileInfo(i);

                            App.Current.Dispatcher.Invoke((Action)delegate
                            {
                                try
                                {
                                    this.Progress(Math.Round(Convert.ToDouble(info.Length) / 1024, 1));
                                }
                                catch (Exception) { }
                            });
                        
                            // Код вязт из StackOverflow, делегату потока диспатчер передаем делегат Action в котором добавляем найденные элементы в результирующую коллекцию
                        App.Current.Dispatcher.Invoke((Action)delegate
                        {
                            result.Add(i); // Забрасываем результат в переменную
                           
                        });


                    }

                }
                catch (Exception e) { Console.WriteLine(e.Message); continue; }
                //Thread.Sleep(10);

            }
        }

        /// <summary>
        /// Расчет размера начальной папки поиска
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="catalogSize"></param>
        /// <returns></returns>
        public static double GetFolderSize(string folder, ref double catalogSize)
        {

            try
            {

                //В переменную catalogSize будем записывать размеры всех файлов, с каждым
                //новым файлом перезаписывая данную переменную
                DirectoryInfo di = new DirectoryInfo(folder);
                DirectoryInfo[] diA = di.GetDirectories();
                FileInfo[] fi = di.GetFiles();
                //В цикле пробегаемся по всем файлам директории di и складываем их размеры
                foreach (FileInfo f in fi)
                {
                    //Записываем размер файла в байтах
                    catalogSize = catalogSize + f.Length;
                }
                //В цикле пробегаемся по всем вложенным директориям директории di 
                foreach (DirectoryInfo df in diA)
                {
                    //рекурсивно вызываем наш метод
                    GetFolderSize(df.FullName, ref catalogSize);
                }
                //1ГБ = 1024 Байта * 1024 КБайта * 1024 МБайта
                return Math.Round(catalogSize / 1024, 1);
            }
            //Начинаем перехватывать ошибки
            //DirectoryNotFoundException - директория не найдена
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
                //Math.Round(catalogSize / 1024 / 1024 / 1024, 1);
            }
            //UnauthorizedAccessException - отсутствует доступ к файлу или папке
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
                    //Math.Round(catalogSize / 1024 / 1024 / 1024, 1);
            }
            //Во всех остальных случаях
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0// Math.Round(catalogSize / 1024 / 1024 / 1024, 1) 
                    ;
            }

        }

    }
}
