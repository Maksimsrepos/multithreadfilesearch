﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MTFS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
        MyThreading threading;
        System.Windows.Data.Binding binding;
        //Search search = new Search();
        private double size = 0;
        public MainWindow()
        {
            InitializeComponent();
            binding = new System.Windows.Data.Binding();
            binding.IsAsync = true;
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            folderBrowser.ShowDialog();
            StartSearchButton.IsEnabled = true;
            Task.Factory.StartNew(() => { Search.GetFolderSize(folder: folderBrowser.SelectedPath, catalogSize: ref size); }).ContinueWith(t => { WorkCompleted(); });
           
           
        }
        /// <summary>
        /// Вызывается по завершению расчета байт (Для реализации прогресса поиска)
        /// </summary>
        private void WorkCompleted()
        {

            Console.WriteLine(size);
           
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            threading = new MyThreading(folderBrowser.SelectedPath, PaternS.Text);
            threading.Create_Threads(Convert.ToInt32(NumericUD.Value));
            ListViewResult.ItemsSource = threading.SearchObj.Result; // тестирую привязку к лист вью элемента ObserbableList
           
        }

        private void Window_Closed(object sender, EventArgs e)
        {

        }

        private void Window_Closed(object sender, System.ComponentModel.CancelEventArgs e)
        {
           
        }
    }
}
